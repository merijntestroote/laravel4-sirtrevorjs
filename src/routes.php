<?php
// Route for upload images
Route::any("/sirtrevorjs/upload", array("uses" => "SirTrevorJsController@upload", 'before' => 'auth'));

// Route for tweets
Route::any("/sirtrevorjs/tweet", array("uses" => "SirTrevorJsController@tweet"));
